package models

import (
	"strings"
)

const (
	// ErrNotFound is returned when a resource cannot be found
	// in the database.
	ErrNotFound modelError = "models: resource not found"

	// ErrIDInvalid is returned when an invalid ID is provided to
	// a method like Delete
	ErrIDInvalid privateError = "models: ID provided was invalid"

	// ErrPasswordWrong is returned when an invalid password
	// is used when attempting to authenticate a user.
	ErrPasswordWrong modelError = "models: wrong password provided"

	// ErrEmailRequired is returned when an email address is
	// not provided when creating a user
	ErrEmailRequired modelError = "models: email address is required"

	// ErrEmailInvalid is returned when an email address provided
	// does not match any of our requirements
	ErrEmailInvalid modelError = "models: email address is not valid"

	// ErrEmailTaken is returned when an update or create id attempted
	// with an email address that is already in use.
	ErrEmailTaken modelError = "models: email address is already taken"

	// ErrPasswordTooShort is returned when a user tried to set a
	// a password that is less than 8 characters
	ErrPasswordTooShort modelError = "models: password must be at least 8 characters long"

	// ErrPasswordRequired is returned when a create is attempted
	// without a user password provided.
	ErrPasswordRequired modelError = "models: password is required"

	// ErrRememberRequired is returned when a create or update
	// is attempted without a user remember token hash
	ErrRememberRequired privateError = "models: remember token is required"

	// ErrRememberTooShort is returned when a remember token is
	// not at least 32 bytes
	ErrRememberTooShort privateError = "models: remember token must be at least 32 bytes"

	// ErrTeamIDRequired is returned when TODO
	ErrTeamIDRequired privateError = "models: team ID is required"

	// ErrTitleRequired is returned when on the form for either
	// InjectTask or InjectResponse are empty.
	ErrTitleRequired modelError = "models: title is required"

	// ErrTokenInvalid is returned when the password reset token does
	// not match for any user.
	//
	// #nosec:G101: look for hardcoded credentials
	ErrTokenInvalid modelError = "models: token provided in not valid."
)

type modelError string

func (e modelError) Error() string {
	return string(e)
}

func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] = strings.Title(split[0])

	return strings.Join(split, " ")
}

type privateError string

func (e privateError) Error() string {
	return string(e)
}
