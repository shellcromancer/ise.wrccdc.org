package models

import (
	"github.com/jinzhu/gorm"

	// postgres drivers are needed but we don't need to use
	// anything from the actual package
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Services is a wrapper object to ease the creation of all
// database services used in the app.
type Services struct {
	User       UserService
	InjectTask InjectTaskService
	db         *gorm.DB
}

// ServiceConfig is the structure that configures our database
type ServiceConfig func(*Services) error

// NewServices instantiates the DB services for all of the models
// used in our services.
func NewServices(cfgs ...ServiceConfig) (*Services, error) {
	var s Services
	for _, cfg := range cfgs {
		if err := cfg(&s); err != nil {
			return nil, err
		}
	}
	return &s, nil
}

// WithGorm will configure the database to connect with the information given
func WithGorm(dialect, connectionInfo string) ServiceConfig {
	return func(s *Services) error {
		db, err := gorm.Open(dialect, connectionInfo)
		if err != nil {
			return err
		}
		s.db = db
		return nil
	}
}

// WithLogMode will set the logging options to the database to whatever
// we pass in.
func WithLogMode(mode bool) ServiceConfig {
	return func(s *Services) error {
		s.db.LogMode(mode)
		return nil
	}
}

// WithUser will setup the UserService with the parameters needed:
// - Pepper for when hashing passwords
// - HMACKey for hashing the remember tokens
func WithUser(pepper, hmacKey string) ServiceConfig {
	return func(s *Services) error {
		s.User = NewUserService(s.db, pepper, hmacKey)
		return nil
	}
}

// WithInjects will setup the InjectsService
func WithInjects() ServiceConfig {
	return func(s *Services) error {
		s.InjectTask = NewInjectTaskService(s.db)
		return nil
	}
}

// Close will clear the database connection
func (s *Services) Close() error {
	return s.db.Close()
}

var tables = []interface{}{
	&User{},
	&InjectTask{},
	&InjectResponse{},
	&pwReset{},
}

// AutoMigrate will perform migrations for all user models
func (s *Services) AutoMigrate() error {
	return s.db.AutoMigrate(tables...).Error
}

// DestructiveReset is a useful dev feature to always work
// with a clean slate
func (s *Services) DestructiveReset() error {
	err := s.db.DropTableIfExists(
		&User{},
		&InjectTask{},
		&InjectResponse{},
		&pwReset{},
	).Error
	if err != nil {
		return err
	}
	return s.AutoMigrate()
}
