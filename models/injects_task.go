package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// InjectTask represents the inject model in our database that
// will be created by a member of Gold Team in an 'admin' page
// and then displayed to blue teams to submit.
type InjectTask struct {
	gorm.Model
	InjectID    uint   `gorm:"not_null;index"`
	Title       string `gorm:"not_null"`
	Value       uint
	Description string
	Deadline    time.Time
}

// InjectTaskService is the usermode interface that controllers &
// main execution threads can use to manipulate injects.
type InjectTaskService interface {
	InjectTaskDB
}

// NewInjectTaskService return the service that the Inject controller
// wields for all of it's magic.
func NewInjectTaskService(db *gorm.DB) InjectTaskService {
	return &injectTaskService{
		InjectTaskDB: &injectTaskValidator{
			InjectTaskDB: &injectTaskGorm{
				db: db,
			},
		},
	}
}

// InjectTaskDB is the interface to interact with the underlying
// database that we are using.
type InjectTaskDB interface {
	ByID(id uint) (*InjectTask, error)
	ByActive(t time.Time) ([]InjectTask, error)
	TaskCreate(inject *InjectTask) error
	TaskUpdate(inject *InjectTask) error
	TaskDelete(id uint) error

	ResponseCreate(response *InjectResponse) error
	ResponseUpdate(response *InjectResponse) error
	ResponseDelete(id uint) error
}

// injectGorm is the the struct responsible for the database layer
type injectTaskGorm struct {
	db *gorm.DB
}

// injectService in the highest level interface layer that implements the
// injectDB interface.
type injectTaskService struct {
	InjectTaskDB
}

// injectValidator fulfills the InjectDB interface and is responsible
// for the validations layer.
type injectTaskValidator struct {
	InjectTaskDB
}

func (iv *injectTaskValidator) CreateTask(inject *InjectTask) error {
	err := runInjectTaskValFn(inject,
		iv.titleRequired,
	)
	if err != nil {
		return err
	}

	return iv.InjectTaskDB.TaskCreate(inject)
}

// TaskCreate will create new inject which should be be submitted by a
// verified member of the Gold/Black team.
func (ig *injectTaskGorm) TaskCreate(inject *InjectTask) error {
	return ig.db.Create(inject).Error
}

// Update is the validation layer to update the InjectTask objects
func (iv *injectTaskValidator) Update(inject *InjectTask) error {
	err := runInjectTaskValFn(inject,
		iv.titleRequired,
	)
	if err != nil {
		return err
	}

	return iv.InjectTaskDB.TaskUpdate(inject)
}

// Update will save the new updated version of the object to the
// database
func (ig *injectTaskGorm) TaskUpdate(inject *InjectTask) error {
	return ig.db.Save(inject).Error
}

func (iv *injectTaskValidator) TaskDelete(id uint) error {
	var inject InjectTask
	inject.ID = id
	err := runInjectTaskValFn(&inject,
		iv.nonZeroID,
	)
	if err != nil {
		return err
	}

	return iv.InjectTaskDB.TaskDelete(id)
}

// Delete will delete the InjectTask with the given ID acting as
// the database layer.
func (ig *injectTaskGorm) TaskDelete(id uint) error {
	inject := InjectTask{
		Model: gorm.Model{
			ID: id,
		},
	}
	return ig.db.Delete(inject).Error
}

// ByActive is the validation layer to make sure bogus doesn't happen
func (iv *injectTaskValidator) ByActive(t time.Time) ([]InjectTask, error) {
	return iv.InjectTaskDB.ByActive(t)
}

// ByActive is the database layer method to return all injects that still
// time remaining.
func (ig *injectTaskGorm) ByActive(t time.Time) ([]InjectTask, error) {
	var injects []InjectTask

	db := ig.db.Where("deadline > ?", t)
	if err := db.Find(&injects).Error; err != nil {
		return nil, err
	}

	return injects, nil
}

func (ig *injectTaskGorm) ByID(id uint) (*InjectTask, error) {
	var inject InjectTask
	db := ig.db.Where("id = ?", id)
	err := first(db, &inject)
	if err != nil {
		return nil, err
	}
	return &inject, nil
}

// injectTaskValFn defines the signatute for my validation functions
type injectTaskValFn func(*InjectTask) error

func runInjectTaskValFn(injectTask *InjectTask, fns ...injectTaskValFn) error {
	for _, fn := range fns {
		if err := fn(injectTask); err != nil {
			return err
		}
	}
	return nil
}

func (iv *injectTaskValidator) titleRequired(i *InjectTask) error {
	if i.Title == "" {
		return ErrTitleRequired
	}
	return nil
}

func (iv *injectTaskValidator) nonZeroID(i *InjectTask) error {
	if i.ID == 0 {
		return ErrIDInvalid
	}
	return nil
}
