package models

// test line to ensure that userGorm implements the UserDB interface
var _ UserDB = &userGorm{}

// test line to ensure that userService fulfills the interface for UserService
var _ UserService = &userService{}
