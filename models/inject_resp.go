package models

import (
	"github.com/jinzhu/gorm"
)

// InjectResponse is the submission that Blue Team competitors
// make for a specific InjectTask.
type InjectResponse struct {
	gorm.Model
	SubmissionID uint `gorm:"not_null;index"`
	TeamID       uint // TODO: verify schema -> `gorm:"not_null"`
	Response     string
	Rating       uint
}

/*
ResponseCreate(response *InjectResponse) error
ResponseUpdate(response *InjectResponse) error
ResponseDelete(id uint) error
*/

func (iv *injectTaskValidator) ResponseCreate(response *InjectResponse) error {
	return iv.InjectTaskDB.ResponseCreate(response)
}

func (ig *injectTaskGorm) ResponseCreate(response *InjectResponse) error {
	return ErrIDInvalid
}

func (iv *injectTaskValidator) ResponseUpdate(response *InjectResponse) error {
	return iv.InjectTaskDB.ResponseUpdate(response)
}

func (ig *injectTaskGorm) ResponseUpdate(response *InjectResponse) error {
	return ErrIDInvalid
}

func (iv *injectTaskValidator) ResponseDelete(id uint) error {
	return iv.InjectTaskDB.ResponseDelete(id)
}

func (ig *injectTaskGorm) ResponseDelete(id uint) error {
	return ErrIDInvalid
}
