package context

import (
	"context"

	"ise.wrccdc.org/models"
)

type privateKey string

const userKey privateKey = "user"

// WithUser adds them to the context
func WithUser(ctx context.Context, user *models.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

// User will fetch the user from the database based on
// the key we stored in hte context.
func User(ctx context.Context) *models.User {
	if temp := ctx.Value(userKey); temp != nil {
		if user, ok := temp.(*models.User); ok {
			return user
		}
	}
	return nil
}
