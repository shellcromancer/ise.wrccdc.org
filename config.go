package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/BurntSushi/toml"
)

// Config configures the port we serve on and the environment this is
type Config struct {
	Port     int
	Env      string
	Pepper   string
	HMACKey  string
	Database PostgresConfig
	Mailgun  MailgunConfig
}

// IsProd checks if we are in development or production
func (c Config) IsProd() bool {
	return c.Env == "prod"
}

// DefaultConfig is the default version of our configurations
func DefaultConfig() Config {
	return Config{
		Port:     3000,
		Env:      "dev",
		Pepper:   "secret-random-string",
		HMACKey:  "secret-hmac-key",
		Database: DefaultPostgresConfig(),
	}
}

// LoadConfig will check if we can load `config.toml` and otherwise
// it will load the default config.
func LoadConfig(configRequired bool) (c Config) {
	tomlData, err := ioutil.ReadFile("config.toml")
	if err != nil {
		if configRequired {
			panic(err)
		}
		log.Println("Using the default config.")
		return DefaultConfig()
	}

	if _, err := toml.Decode(string(tomlData), &c); err != nil {
		log.Fatalln(err)
	}
	return c
}

// PostgresConfig is the struct that contains data needed to
// build working Postgres connections.
type PostgresConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	Name     string
}

// Dialect returns the database type for gorm to make some
// sense of
func (c PostgresConfig) Dialect() string {
	return "postgres"
}

// ConnectionInfo builds the string with details needed to
// connect to the database.
func (c PostgresConfig) ConnectionInfo() string {
	if c.Password == "" {
		return fmt.Sprintf("host=%s port=%d user=%s dbname=%s "+
			"sslmode=disable", c.Host, c.Port, c.User, c.Name)
	}

	return fmt.Sprintf("host=%s port=%d user=%s password=%s "+
		"dbname=% sslmode=disable", c.Host, c.Port, c.User,
		c.Password, c.Name)
}

// DefaultPostgresConfig returns a basic set of settings to make shit work
func DefaultPostgresConfig() PostgresConfig {
	return PostgresConfig{
		Host:     "localhost",
		Port:     5432,
		User:     "postgres",
		Password: "",
		Name:     "ccdc_ise_dev",
	}
}

// MailgunConfig is the struct that contains data needed to
// build working Mailgun emails.
type MailgunConfig struct {
	APIKey       string
	PublicAPIKey string
	Domain       string
}
