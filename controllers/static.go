package controllers

import (
	"ise.wrccdc.org/views"
)

// Static is the type of the static controller
type Static struct {
	Home    *views.View
	Contact *views.View
}

// NewStatic returns a new static controller.
func NewStatic() *Static {
	return &Static{
		Home:    views.NewView("bootstrap", "static/home"),
		Contact: views.NewView("bootstrap", "static/contact")}
}
