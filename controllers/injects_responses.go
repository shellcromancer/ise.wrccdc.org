package controllers

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"ise.wrccdc.org/context"
	"ise.wrccdc.org/models"
	"ise.wrccdc.org/views"
)

const (
	maxMultipartMem = 1 << 20 // 1 megabyte

	// ShowInjectResponse is the named route for showing an inject
	ShowInjectResponse = "show_response"
)

type injectResponseForm struct {
	Response string `schema:"response"`
	Rating   uint   `schema:"rating"`
}

// ResponseCreate is
//
// POST /injects/:ID/responses/create
func (i *Injects) ResponseCreate(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data

	inject, err := i.injectByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	var form injectResponseForm
	if err := parseForm(r, &form); err != nil {
		viewData.SetAlert(err)
		i.TaskCreateView.Render(w, r, viewData)
		return
	}

	response := models.InjectResponse{
		TeamID:   user.TeamID,
		Response: form.Response,
		Rating:   form.Rating,
	}

	if err := i.is.ResponseCreate(&response); err != nil {
		viewData.SetAlert(err)
		i.TaskCreateView.Render(w, r, viewData)
		return
	}

	url, err := i.r.Get(ShowInjectResponse).URL(
		"id", strconv.Itoa(int(inject.ID)),
		"resp", strconv.Itoa(int(response.ID)),
	)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	http.Redirect(w, r, url.Path, http.StatusFound)
}

// FileUpload processes the form submission for files
func (i *Injects) FileUpload(w http.ResponseWriter, r *http.Request) {
	inject, err := i.injectByID(w, r)
	if err != nil {
		return
	}
	_ = context.User(r.Context())

	var viewData views.Data
	viewData.Yield = inject
	err = r.ParseMultipartForm(maxMultipartMem)
	if err != nil {
		viewData.SetAlert(err)
		i.ResponseUpdateView.Render(w, r, viewData)
		return
	}

	// create directory that holds our files
	filePath := filepath.Join("files", "injects",
		fmt.Sprintf("%v", inject.ID))
	err = os.MkdirAll(filePath, 0755)
	if err != nil {
		viewData.SetAlert(err)
		i.ResponseUpdateView.Render(w, r, viewData)
		return
	}

	files := r.MultipartForm.File["files"]
	for _, f := range files {
		// open the uploaded files
		file, err := f.Open()
		if err != nil {
			viewData.SetAlert(err)
			i.ResponseUpdateView.Render(w, r, viewData)
			return
		}
		defer file.Close()

		dst, err := os.Create(filepath.Join(filePath, f.Filename))
		if err != nil {
			viewData.SetAlert(err)
			i.ResponseUpdateView.Render(w, r, viewData)
			return
		}
		defer dst.Close()

		_, err = io.Copy(dst, file)
		if err != nil {
			viewData.SetAlert(err)
			i.ResponseUpdateView.Render(w, r, viewData)
			return
		}
	}

	viewData.Alert = &views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Files uploaded successfully!",
	}
	i.ResponseUpdateView.Render(w, r, viewData)
}
