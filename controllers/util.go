package controllers

import (
	"net/http"
	"net/url"

	"github.com/gorilla/schema"
)

// parseForm will decode the http FormBody into the destination
// interface.
func parseForm(r *http.Request, dest interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	return parseValues(r.PostForm, dest)
}

// parseURLParams will decode URL parameters into the destination
// interface.
func parseURLParams(r *http.Request, dest interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	return parseValues(r.Form, dest)
}

// parseValues can will take url encoded values and wrangle them
// into the arbitrary interfaces provided they are given tags.
func parseValues(values url.Values, dest interface{}) error {
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	if err := dec.Decode(dest, values); err != nil {
		return err
	}
	return nil
}
