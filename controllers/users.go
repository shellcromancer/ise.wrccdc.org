package controllers

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"ise.wrccdc.org/context"
	"ise.wrccdc.org/email"
	"ise.wrccdc.org/models"
	"ise.wrccdc.org/rand"
	"ise.wrccdc.org/views"
)

// Users is the type for a user controller.
type Users struct {
	RegisterView *views.View
	LoginView    *views.View
	ForgotPwView *views.View
	ResetPwView  *views.View
	us           models.UserService
	emailer      *email.Client
}

// NewUsers will return the Users Controller.
func NewUsers(us models.UserService, emailer *email.Client) *Users {
	return &Users{
		RegisterView: views.NewView("bootstrap", "users/register"),
		LoginView:    views.NewView("bootstrap", "users/login"),
		ForgotPwView: views.NewView("bootstrap", "users/forgot_pw"),
		ResetPwView:  views.NewView("bootstrap", "users/reset_pw"),
		us:           us,
		emailer:      emailer,
	}
}

// Register is used to render the form where a user can create
// a new user account.
//
// GET /register
func (u *Users) Register(w http.ResponseWriter, r *http.Request) {
	var form registrationForm
	err := parseURLParams(r, &form)
	if err != nil {
		var viewData views.Data
		log.Println(err)
		viewData.SetAlert(err)
		u.RegisterView.Render(w, r, viewData)
		return
	}
	u.RegisterView.Render(w, r, form)
}

// registrationForm is the struct containing fields in the form at /register
type registrationForm struct {
	Name     string `schema:"name"`
	Email    string `schema:"email"`
	TeamID   uint   `schema:"teamID"`
	Password string `schema:"password"`
	Role     string `schema:"role"`
}

// Create is used to process the form where a user can create
// a new user account.
//
// GET /register
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	form := registrationForm{}
	viewData.Yield = &form

	if err := parseForm(r, &form); err != nil {
		log.Println(err)
		viewData.SetAlert(err)
		u.RegisterView.Render(w, r, viewData)
		return
	}

	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		TeamID:   form.TeamID,
		Password: form.Password,
		Role:     form.Role,
	}

	if err := u.us.Create(&user); err != nil {
		viewData.SetAlert(err)
		u.RegisterView.Render(w, r, viewData)
		return
	}

	err := u.emailer.WelcomeEmail(form.Email)
	if err != nil {
		viewData.SetAlert(err)
		u.ForgotPwView.Render(w, r, viewData)
		return
	}

	err = u.signIn(w, &user)
	if err != nil {
		viewData.SetAlert(err)
		u.LoginView.Render(w, r, viewData)
		return
	}

	views.RedirectALert(w, r, "/injects", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "You have been logged in!",
	})
}

// loginForm is the struct containing fields in the form at /login
type loginForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// Login is used to process the form where a user can login
// to their existing user account.
//
// POST /login
func (u *Users) Login(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	form := loginForm{}

	if err := parseForm(r, &form); err != nil {
		viewData.SetAlert(err)
		u.LoginView.Render(w, r, viewData)
		return
	}

	user, err := u.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			viewData.AlertError("No user exists with that email address")
		default:
			viewData.SetAlert(err)
		}
		u.LoginView.Render(w, r, viewData)
	}

	err = u.signIn(w, user)
	if err != nil {
		viewData.SetAlert(err)
		u.LoginView.Render(w, r, viewData)
		return
	}

	views.RedirectALert(w, r, "/injects", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "You have been logged in!",
	})
}

// Logout is used to delete a users session cookie and invalidate
// their current remember token, which will sign the current user
// out.
//
// POST /logout
func (u *Users) Logout(w http.ResponseWriter, r *http.Request) {
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
	}

	http.SetCookie(w, &cookie)

	user := context.User(r.Context())
	var viewData views.Data

	token, err := rand.RememberToken()
	if err != nil {
		viewData.SetAlert(err)
		u.LoginView.Render(w, r, viewData)
		return
	}

	user.Remember = token
	err = u.us.Update(user)
	if err != nil {
		viewData.SetAlert(err)
		u.LoginView.Render(w, r, viewData)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

type resetPwForm struct {
	Email    string
	Token    string
	Password string
}

// InitiateReset will start the password reset process by handling the
// form data, triggering the token generation & sending this to the
// user.
//
// POST /forgot
func (u *Users) InitiateReset(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	var form resetPwForm
	viewData.Yield = &form
	if err := parseForm(r, &form); err != nil {
		viewData.SetAlert(err)
		u.ForgotPwView.Render(w, r, viewData)
		return
	}

	token, err := u.us.InitateReset(form.Email)
	if err != nil {
		viewData.SetAlert(err)
		u.ForgotPwView.Render(w, r, viewData)
		return
	}

	err = u.emailer.ResetPasswordEmail(form.Email, token)
	if err != nil {
		viewData.SetAlert(err)
		u.ForgotPwView.Render(w, r, viewData)
		return
	}

	views.RedirectALert(w, r, "/reset", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Instructions for resetting your password have been emailed to you.",
	})
}

// ResetPw displays the password reset form and has a method
// so we can prefill the form data with a token provided via the
// URL query parameters
//
// GET /reset
func (u *Users) ResetPw(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	var form resetPwForm
	viewData.Yield = &form
	if err := parseURLParams(r, &form); err != nil {
		viewData.SetAlert(err)
	}
	u.ResetPwView.Render(w, r, viewData)
}

// CompleteReset processes the reset password form.
//
// POST /reset
func (u *Users) CompleteReset(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	var form resetPwForm
	viewData.Yield = &form
	if err := parseForm(r, &form); err != nil {
		viewData.SetAlert(err)
		u.ResetPwView.Render(w, r, viewData)
		return
	}

	user, err := u.us.CompleteReset(form.Token, form.Password)
	if err != nil {
		viewData.SetAlert(err)
		u.ResetPwView.Render(w, r, viewData)
		return
	}

	err = u.signIn(w, user)
	if err != nil {
		viewData.SetAlert(err)
		u.LoginView.Render(w, r, viewData)
		return
	}

	views.RedirectALert(w, r, "/injects", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Your password has been reset and you have been logged in!",
	})
}

// signIn is used to sign in the give user via cookies
func (u *Users) signIn(w http.ResponseWriter, user *models.User) error {
	//TODO: Implement this
	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
		err = u.us.Update(user)
		if err != nil {
			return err
		}
	}

	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    user.Remember,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)

	return nil
}

// ---- TODO: This will be deleted later

// CookieTest is used to display the current users cookies
func (u *Users) CookieTest(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("remember_token")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user, err := u.us.ByRemember(cookie.Value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintln(w, "User is:", user.Name)
}
