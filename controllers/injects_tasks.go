package controllers

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"ise.wrccdc.org/context"
	"ise.wrccdc.org/models"
	"ise.wrccdc.org/views"
)

const (
	// ShowInjectTask is the named route for the one that displays the
	// single inject.
	ShowInjectTask = "show_inject"
	// UpdateInjectTask is the named route for ...
	UpdateInjectTask = "update_inject"
	// IndexInjects is the named route for all inject routes
	IndexInjects = "index_injects"
)

// Injects is our controlling handle for the inject related paths
type Injects struct {
	IndexView *views.View

	// Views defined in this file for rendering.
	TaskCreateView *views.View
	TaskUpdateView *views.View
	TaskShowView   *views.View

	// ResponseCreate is the view that is the inject submission
	// form view.
	ResponseCreateView *views.View
	// ResponseUpdate is the view that is the inject editing
	// form view.
	ResponseUpdateView *views.View
	// ResponseShow is the view that shows the response that the
	// team submitted for the inject.
	ResponseShowView *views.View

	// is is the the database service for the Injects table
	is models.InjectTaskService
	// r is the global router so we can get named routes
	r *mux.Router
}

// NewInjects will create the basic injects handle which populates
// it's views and model.Service
func NewInjects(is models.InjectTaskService, r *mux.Router) *Injects {
	return &Injects{
		IndexView:          views.NewView("bootstrap", "injects/index"),
		TaskShowView:       views.NewView("bootstrap", "injects/tasks/show"),
		TaskCreateView:     views.NewView("bootstrap", "injects/tasks/create"),
		TaskUpdateView:     views.NewView("bootstrap", "injects/tasks/update"),
		ResponseShowView:   views.NewView("bootstrap", "injects/responses/show"),
		ResponseCreateView: views.NewView("bootstrap", "injects/responses/create"),
		ResponseUpdateView: views.NewView("bootstrap", "injects/responses/update"),
		is:                 is,
		r:                  r,
	}
}

func (i *Injects) injectByID(w http.ResponseWriter, r *http.Request) (*models.InjectTask, error) {
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid Inject ID. Don't work ahead.", http.StatusNotFound)
		return nil, err
	}
	inject, err := i.is.ByID(uint(id))
	if err != nil {
		switch err {
		case models.ErrNotFound:
			http.Error(w, "Inject not found.", http.StatusNotFound)
		default:
			http.Error(w, "Welp. That's an error.", http.StatusInternalServerError)
		}
	}
	return inject, nil
}

// Index is the banner page for all injects
//
// GET /injects
func (i *Injects) Index(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	activeInjects, err := i.is.ByActive(time.Now())
	if err != nil {
		viewData.SetAlert(err)
		i.IndexView.Render(w, r, viewData)
		return
	}

	viewData.Yield = activeInjects
	i.IndexView.Render(w, r, viewData)
}

type injectTaskForm struct {
	Title       string    `schema:"title"`
	Value       uint      `schema:"value"`
	Description string    `schema:"description"`
	Deadline    time.Time `schema:"deadline"`
}

// CreateTask process the injectTask submission form from Gold/White team.
//
// POST /injects/create
func (i *Injects) CreateTask(w http.ResponseWriter, r *http.Request) {
	var viewData views.Data
	var form injectTaskForm

	if err := parseForm(r, &form); err != nil {
		viewData.SetAlert(err)
		i.TaskCreateView.Render(w, r, viewData)
		return
	}

	inject := models.InjectTask{
		Title:       form.Title,
		Value:       form.Value,
		Description: form.Description,
		Deadline:    time.Now().AddDate(0, 0, 1),
	}

	if err := i.is.TaskCreate(&inject); err != nil {
		viewData.SetAlert(err)
		i.TaskCreateView.Render(w, r, viewData)
		return
	}

	// create a URL using the router & named ShowInject route
	url, err := i.r.Get(UpdateInjectTask).URL("id",
		strconv.Itoa(int(inject.ID)))
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// if there are no errors, use the URL we just created &
	// redirect to the path portion of the URL. We don't need
	// the whole URL because the application can have an
	// arbitrary hostname so we are transparent to that part.
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// ShowTask will display one inject task that has been created.
//
// GET /injects/:id
func (i *Injects) ShowTask(w http.ResponseWriter, r *http.Request) {
	inject, err := i.injectByID(w, r)
	if err != nil {
		return
	}

	var viewData views.Data
	viewData.Yield = inject

	i.TaskShowView.Render(w, r, viewData)
}

// ShowUpdateTask will show the UpdateTask view after populating the form
// information.
//
// GET /injects/:id/update
func (i *Injects) ShowUpdateTask(w http.ResponseWriter, r *http.Request) {
	inject, err := i.injectByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	if !isAdmin(user.Role) {
		http.Error(w, "You don't have permission to edit injects.", http.StatusForbidden)
		return
	}

	var viewData views.Data
	viewData.Yield = inject
	i.TaskUpdateView.Render(w, r, viewData)
}

// UpdateTask will
//
// POST /injects/:id/update
func (i *Injects) UpdateTask(w http.ResponseWriter, r *http.Request) {
	inject, err := i.injectByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if !isAdmin(user.Role) {
		http.Error(w, "You don't have permission to edit injects.", http.StatusForbidden)
	}

	var viewData views.Data
	viewData.Yield = inject

	var form injectTaskForm
	if err = parseForm(r, &form); err != nil {
		viewData.SetAlert(err)
		i.TaskUpdateView.Render(w, r, viewData)
	}

	// TODO: find a better method that individual copy
	inject.Title = form.Title
	inject.Value = form.Value
	inject.Description = form.Description
	inject.Deadline = form.Deadline

	err = i.is.TaskUpdate(inject)
	if err != nil {
		viewData.SetAlert(err)
		i.TaskUpdateView.Render(w, r, viewData)
		return
	}
	viewData.Alert = &views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Inject successfully updated!",
	}

	i.TaskUpdateView.Render(w, r, viewData)
}

// DeleteTask is the post handler for deleting the InjectTask
//
// POST /injects/:id/delete
func (i *Injects) DeleteTask(w http.ResponseWriter, r *http.Request) {
	inject, err := i.injectByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())

	if !isAdmin(user.Role) {
		http.Error(w, "You don't have permission to delete injects.", http.StatusForbidden)
	}

	var viewData views.Data
	err = i.is.TaskDelete(inject.ID)
	if err != nil {
		viewData.SetAlert(err)
		viewData.Yield = inject
		i.TaskUpdateView.Render(w, r, viewData)
		return
	}

	url, err := i.r.Get(IndexInjects).URL()
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

func isAdmin(role string) bool {
	var admins = []string{
		"Black Team",
		"White Team",
		"Gold Team",
	}

	for _, r := range admins {
		if strings.EqualFold(r, role) {
			return true
		}
	}
	return false
}
