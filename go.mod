module ise.wrccdc.org

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gorilla/csrf v1.6.2
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.1.0
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.3.0 // indirect
	github.com/mailgun/mailgun-go/v3 v3.6.4
	go.opencensus.io v0.22.3
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17
)

go 1.13
