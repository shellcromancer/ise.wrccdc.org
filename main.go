package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"

	"ise.wrccdc.org/controllers"
	"ise.wrccdc.org/email"
	"ise.wrccdc.org/middleware"
	"ise.wrccdc.org/models"
	"ise.wrccdc.org/rand"
)

func main() {
	boolPtr := flag.Bool("prod", false, "Provide this flag in "+
		"production. This ensures that a config.toml file is "+
		"provided before the application starts.")
	flag.Parse()

	cfg := LoadConfig(*boolPtr)
	dbCfg := cfg.Database

	services, err := models.NewServices(
		models.WithGorm(dbCfg.Dialect(), dbCfg.ConnectionInfo()),
		models.WithLogMode(!cfg.IsProd()),
		models.WithUser(cfg.Pepper, cfg.HMACKey),
		models.WithInjects(),
	)
	if err != nil {
		panic(err)
	}
	defer services.Close()

	err = services.DestructiveReset()
	//err = services.AutoMigrate()
	if err != nil {
		panic(err)
	}

	userMw := middleware.User{
		UserService: services.User,
	}

	mg := cfg.Mailgun
	emailer := email.NewClient(
		email.WithSender("Shellcromancer", "black-team@wrccdc.org"),
		email.WithMailgun(mg.Domain, mg.APIKey, mg.PublicAPIKey),
	)

	r := setupRouter(services, emailer)
	authKey, err := rand.Bytes(32)
	if err != nil {
		panic(err)
	}
	csrfMw := csrf.Protect(authKey,
		//csrf.Secure(cfg.IsProd()), TODO: fix csrf.Secure(true)
		csrf.Secure(false),
		csrf.HttpOnly(cfg.IsProd()),
		csrf.SameSite(csrf.SameSiteStrictMode),
	)

	addr := fmt.Sprintf("localhost:%d", cfg.Port)
	fmt.Printf("Serving my app on http://%s\n", addr)

	err = http.ListenAndServe(addr, csrfMw(userMw.Apply(r)))
	if err != nil {
		panic(err)
	}
}

func lostPage(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	w.Header().Set("Content-Type", "text/html")

	fmt.Fprint(w, "It looks like you're lost. "+
		"Let Black Team know if you need help")
}

var lostHandler http.Handler = http.HandlerFunc(lostPage)

func setupRouter(svc *models.Services, emailer *email.Client) *mux.Router {
	assetHandler := http.FileServer(http.Dir("./assets/"))
	assetHandler = http.StripPrefix("/assets/", assetHandler)
	requireUserMw := middleware.RequireUser{}

	r := mux.NewRouter()
	r.NotFoundHandler = lostHandler
	r.PathPrefix("/assets/").Handler(assetHandler)

	staticC := controllers.NewStatic()
	usersC := controllers.NewUsers(svc.User, emailer)
	injectsC := controllers.NewInjects(svc.InjectTask, r)

	r.Handle("/", staticC.Home).Methods("GET")
	r.Handle("/contact", staticC.Contact).Methods("GET")

	// User related routes:
	r.HandleFunc("/register", usersC.Register).Methods("GET")
	r.HandleFunc("/register", usersC.Create).Methods("POST")
	r.Handle("/login", usersC.LoginView).Methods("GET")
	r.HandleFunc("/login", usersC.Login).Methods("POST")
	r.HandleFunc("/logout",
		requireUserMw.ApplyFn(usersC.Logout)).
		Methods("POST")
	r.Handle("/forgot", usersC.ForgotPwView).
		Methods("GET")
	r.HandleFunc("/forgot", usersC.InitiateReset).
		Methods("POST")
	r.HandleFunc("/reset", usersC.ResetPw).
		Methods("GET")
	r.HandleFunc("/reset", usersC.CompleteReset).
		Methods("POST")

	// Middleware inserted for pages that require login
	r.HandleFunc("/injects",
		requireUserMw.ApplyFn(injectsC.Index)).
		Methods("GET").Name(controllers.IndexInjects)
	r.HandleFunc("/injects/create",
		requireUserMw.Apply(injectsC.TaskCreateView)).
		Methods("GET")
	r.HandleFunc("/injects/create",
		requireUserMw.ApplyFn(injectsC.CreateTask)).
		Methods("POST")
	r.HandleFunc("/injects/{id:[0-9]+}", injectsC.ShowTask).
		Methods("GET").Name(controllers.ShowInjectTask)
	r.HandleFunc("/injects/{id:[0-9]+}/update",
		requireUserMw.ApplyFn(injectsC.ShowUpdateTask)).
		Methods("GET").Name(controllers.UpdateInjectTask)
	r.HandleFunc("/injects/{id:[0-9]+}/update",
		requireUserMw.ApplyFn(injectsC.UpdateTask)).
		Methods("POST")
	r.HandleFunc("/injects/{id:[0-9]+}/delete",
		requireUserMw.ApplyFn(injectsC.DeleteTask)).
		Methods("POST")

	// TODO: this is experimental
	r.HandleFunc("/injects/{id:[0-9]+}/responses/create",
		requireUserMw.Apply(injectsC.ResponseCreateView)).
		Methods("GET")
	r.HandleFunc("/injects/{id:[0-9]+}/responses/create",
		requireUserMw.ApplyFn(injectsC.ResponseCreate)).
		Methods("POST")

	return r
}
