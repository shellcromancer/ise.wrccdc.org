package views

import (
	"bytes"
	"errors"
	"html/template"
	"io"
	"log"
	"net/http"
	"path/filepath"

	"github.com/gorilla/csrf"
	"ise.wrccdc.org/context"
)

const (
	layoutDir   string = "views/layouts/"
	templateDir string = "views/"
	templateExt string = ".gohtml"
)

// View is the struct for a view lol
type View struct {
	Template *template.Template
	Layout   string
}

// NewView renders a new view with things
func NewView(layout string, files ...string) *View {
	addTemplatePath(files)
	addTemplateExt(files)
	files = append(files, layoutFiles()...)

	t, err := template.New("").Funcs(template.FuncMap{
		"csrfField": func() (template.HTML, error) {
			return "", errors.New("csrfField is not implemented")
		},
	}).ParseFiles(files...)
	if err != nil {
		panic(err)
	}

	return &View{
		Template: t,
		Layout:   layout,
	}
}

// Render is user to render the view the predefined layout
func (v *View) Render(w http.ResponseWriter, r *http.Request, data interface{}) {
	w.Header().Set("Content-Type", "text/html")
	var viewData Data

	switch d := data.(type) {
	case Data:
		viewData = d
	default:
		viewData = Data{
			Yield: data,
		}
	}
	if alert := getAlert(r); alert != nil {
		viewData.Alert = alert
		clearAlert(w)
	}

	// Lookup and set the user to the User field
	viewData.User = context.User(r.Context())

	// Set the CSRF Field
	csrfField := csrf.TemplateField(r)
	tpl := v.Template.Funcs(template.FuncMap{
		"csrfField": func() template.HTML {
			return csrfField
		},
	})

	var buf bytes.Buffer
	err := tpl.ExecuteTemplate(&buf, v.Layout, viewData)
	if err != nil {
		log.Println(err)
		http.Error(w, "Something went wrong. If the error persists, "+
			"please contact the Black Team.", http.StatusInternalServerError)
		return
	}

	_, err = io.Copy(w, &buf)
	if err != nil {
		panic(err)
	}
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v.Render(w, r, nil)
}

func layoutFiles() []string {
	files, err := filepath.Glob(layoutDir + "*" + templateExt)
	if err != nil {
		panic(err)
	}
	return files
}

func addTemplatePath(files []string) {
	for i, f := range files {
		files[i] = templateDir + f
	}
}

func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + templateExt
	}
}
