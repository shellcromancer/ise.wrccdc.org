package views

import (
	"log"
	"net/http"
	"time"

	"ise.wrccdc.org/models"
)

// Data is the top level structure that views expect data
// to come in as Yield, and required User with optional
// Alert type.
type Data struct {
	Alert *Alert
	User  *models.User
	Yield interface{}
}

// Alert is used to render Bootstrap Alert messages in templates
type Alert struct {
	Level   string
	Message string
}

// PublicError are ones we are okay with surfacing to the end
// user of our application
type PublicError interface {
	error
	Public() string
}

// SetAlert will create a message type of alert with this error
// and is intended for the Public error method. Otherwise we simplify
// to a generic message.
func (d *Data) SetAlert(err error) {
	var msg string
	if pErr, ok := err.(PublicError); ok {
		msg = pErr.Public()
	} else {
		log.Println(err)
		msg = AlertMsgGeneric
	}

	d.Alert = &Alert{
		Level:   AlertLvlError,
		Message: msg,
	}
}

// AlertError sets the error message to the param.
func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLvlError,
		Message: msg,
	}
}

// RedirectALert accepts all the normal parameters for an
// http.Redirect and reforms a redirect, but only after persisting
// the provided alert in a cookie so that it can be displayed
// when the new page is loaded.
func RedirectALert(w http.ResponseWriter, r *http.Request, urlStr string, code int, alert Alert) {
	persistAlert(w, alert)
	http.Redirect(w, r, urlStr, code)
}

// persistAlert is used to set an alert on a response that will
// stick across redirects (up to 5 mins).
func persistAlert(w http.ResponseWriter, alert Alert) {
	expiresAt := time.Now().Add(5 * time.Minute)
	lvl := http.Cookie{
		Name:     "alert_level",
		Value:    alert.Level,
		Expires:  expiresAt,
		HttpOnly: true,
	}
	msg := http.Cookie{
		Name:     "alert_message",
		Value:    alert.Message,
		Expires:  expiresAt,
		HttpOnly: true,
	}
	http.SetCookie(w, &lvl)
	http.SetCookie(w, &msg)
}

// clearAlert will clear alerts that have been set before their
// 5 min lifetime has expired.
func clearAlert(w http.ResponseWriter) {
	lvl := http.Cookie{
		Name:     "alert_level",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
	}
	msg := http.Cookie{
		Name:     "alert_message",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
	}
	http.SetCookie(w, &lvl)
	http.SetCookie(w, &msg)
}

// getAlert is a helper method to fetch the alert cookies we
// persisted with view.RedirectAlert.
func getAlert(r *http.Request) *Alert {
	lvl, err := r.Cookie("alert_level")
	if err != nil {
		return nil
	}
	msg, err := r.Cookie("alert_message")
	if err != nil {
		return nil
	}

	alert := Alert{
		Level:   lvl.Value,
		Message: msg.Value,
	}
	return &alert
}

const (
	// AlertLvlError is a bootstrap alert constant for danger
	AlertLvlError = "danger"
	// AlertLvlWarning is a bootstrap alert constant for warning
	AlertLvlWarning = "warning"
	// AlertLvlInfo is a bootstrap alert constant for info
	AlertLvlInfo = "info"
	// AlertLvlSuccess is a bootstrap alert constant for success
	AlertLvlSuccess = "success"

	// AlertMsgGeneric is displayed when any random error is
	// encountered in our backend
	AlertMsgGeneric = "Something went wrong. Please try " +
		"again, and contact Black Team if the problem persists"
)
