# CCDC Portal: ise.wrccdc.org
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/shellcromancer/ise.wrccdc.org)](https://goreportcard.com/report/gitlab.com/shellcromancer/ise.wrccdc.org)

## Development

This project has been developed with the version `Golang 1.13`, and the dependencies are using the native Go modules. Before pushing code it's important to run `golangci-lint run` to catch any unexpected errors in addition to manually testing any changes made.

### Getting it running:

#### Before you start:

To kick things off you'll need a installation of:

* Golang
* Postgres

Then the real fun begins:

```bash
cp config.toml.dist config.toml
vim config.toml # Edit this to add your production parameters
go run main.go config.go
```

### Postgres


## Documentation

To view the auto-generated docs run:

```
godoc -http=:6060
```

And then navigate over to:

http://localhost:6060/pkg/github.com/shellcromancer/ise.wrccdc.org/models/