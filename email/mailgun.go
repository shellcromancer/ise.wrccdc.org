package email

import (
	"context"
	"fmt"
	"log"
	"net/url"

	mailgun "github.com/mailgun/mailgun-go/v3"
)

const (
	welcomeSubject = "[WRCCDC] Welcome to ise.wrccdc.org!"
	resetSubject   = "[WRCCDC] Instructions for reseting your password."
	resetBaseURL   = "http://localhost:3000/reset"
)

const welcomeText = `
Hi there!

Welcome to ise.wrccdc.org! We really hope you enjoy the upcoming competition!

Best,
Black Team
`

const resetTextTemplate = `
It appears that you have requested a password reset. 
If this was you, please follow the link below to update your password:
%s

If you are asked for a token, please use the following value:
%s

If you didn't request a password reset you can safely ignore this email and your account will not be changed.

Best,
Black Team
`

// WithMailgun TODO: docs
func WithMailgun(domain, apiKey, publicKey string) ClientConfig {
	return func(c *Client) {
		mg := mailgun.NewMailgun(domain, apiKey)
		c.mg = mg
	}
}

// WithSender TODO: docs
func WithSender(name, email string) ClientConfig {
	return func(c *Client) {
		c.from = buildEmail(name, email)
	}
}

// NewClient TODO: docs
func NewClient(opts ...ClientConfig) *Client {
	client := Client{
		// set default from address
		from: "support@wrccdc.org",
	}

	for _, opt := range opts {
		opt(&client)
	}
	return &client
}

// WelcomeEmail TODO: docs
func (c *Client) WelcomeEmail(toEmail string) error {
	msg := c.mg.NewMessage(
		c.from,
		welcomeSubject,
		welcomeText,
		toEmail,
	)

	_, id, err := c.mg.Send(context.Background(), msg)
	if err != nil {
		return err
	}
	log.Printf("Sent welcome email to %s. id=%s\n", toEmail, id)

	return nil
}

// ResetPasswordEmail TODO: docs
func (c *Client) ResetPasswordEmail(toEmail, token string) error {
	v := url.Values{}
	v.Set("token", token)
	resetURL := "?" + v.Encode()

	fmt.Println(resetURL)
	resetText := fmt.Sprintf(resetTextTemplate, resetURL, token)

	msg := c.mg.NewMessage(
		c.from,
		resetSubject,
		resetText,
		toEmail,
	)
	_, id, err := c.mg.Send(context.Background(), msg)
	if err != nil {
		return err
	}

	log.Printf("Send a password reset email. to=%s id=%s", toEmail, id)
	return nil
}

// ClientConfig TODO: docs
type ClientConfig func(*Client)

// Client TODO: docs
type Client struct {
	from string
	mg   mailgun.Mailgun
}

func buildEmail(name, email string) string {
	if name == "" {
		return email
	}
	return fmt.Sprintf("%s <%s>", name, email)
}
